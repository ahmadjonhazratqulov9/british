from django.shortcuts import render, redirect
from .models import *


def IndexView(request):
    about = About.objects.all()
    courses = Courses.objects.all()
    teacher = Teacher.objects.all()
    contact = Contact.objects.all()
    context = {

        'about': about,
        'courses': courses,
        'teacher': teacher,
        'contact': contact,

    }
    return render(request, 'index.html', context)


def AboutView(request):
    about = About.objects.all()
    galery = Galeriya.objects.all()
    contact = Contact.objects.all()
    context = {
        'about': about,
        'galery': galery,
        'contact': contact,
    }
    return render(request, 'about.html', context)



def CoursesView(request):
    # language = Language.objects.all()
    # lang_category = Lang_Category.objects.all()
    # about_lang = AboutLanguage.objects.all()
    contact = Contact.objects.all()
    context = {
        'contact': contact,


        # 'language': language,
        # 'lang_ctegory': lang_category,
        # 'about_lang': about_lang,

    }
    return render(request, 'courses.html', context)



def ContactView(request):
    contact = Contact.objects.all()
    context = {
        'contact': contact,
    }

    return render(request, 'contact.html', context)


def Message(request):

    if request.method == 'POST':
        name = request.POST['name']
        full_name = request.POST['full_name']
        phone_number = request.POST['phone_number']
        message = request.POST['message']
        m = Register.objects.create(
            name=name,
            full_name=full_name,
            phone_number=phone_number,
            message=message,
        )
        m.save()
        return redirect('index')


