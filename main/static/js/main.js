function openNav() {
    document.querySelector('.navbar').classList.toggle("navbar-active");
    document.querySelector(".toggle").classList.toggle("btn-active");
}
window.addEventListener('scroll', function () {
    const header = document.querySelector('header');
    header.classList.toggle('sticky', window.scrollY > 1000);
});
function dnone(){
    document.querySelector('.regist').classList.add("none");
}