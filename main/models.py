from django.db import models


class About(models.Model):
    title = models.CharField(max_length=500)
    text = models.TextField()
    image = models.ImageField(upload_to='media')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Biz haqimizda"


class Courses(models.Model):
    image = models.ImageField(upload_to='media')
    course_name = models.CharField(max_length=255)
    text = models.TextField()

    def __str__(self):
        return self.course_name

    class Meta:
        verbose_name_plural = "Bosh sahifa joylashgan kursalar"


class Teacher(models.Model):
    image = models.ImageField(upload_to='media')
    full_name = models.CharField(max_length=500)
    yunalishi = models.CharField(max_length=500)
    text = models.TextField()

    def __str__(self):
        return self.full_name

    class Meta:
        verbose_name_plural = "O`qituvchilar haqida"


class Register(models.Model):
    name = models.CharField(max_length=255)
    full_name = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=255)
    message = models.TextField()

    def __str__(self):
        return f"{self.name}  {self.full_name} | {self.phone_number}"

    class Meta:
        verbose_name_plural = "Habarlar"


class Contact(models.Model):
    manzil = models.CharField(max_length=5000)
    email = models.CharField(max_length=500)
    phone_number = models.CharField(max_length=255)
    facebook = models.CharField(max_length=255)
    you_tube = models.CharField(max_length=255)
    telegram = models.CharField(max_length=255)
    tik_tok = models.CharField(max_length=255)
    instagram = models.CharField(max_length=255)

    def __str__(self):
        return self.manzil

    class Meta:
        verbose_name_plural = "Kontakt"


class Galeriya(models.Model):
    image = models.ImageField(upload_to='galeriya/')


    class Meta:
        verbose_name_plural = "Galeriya"












#
# class Language(models.Model):
#     til_nomi = models.CharField(max_length=255)
#
#     def __str__(self):
#         return self.til_nomi
#
#     class Meta:
#         verbose_name_plural = "Mavjud Tillar"
#
#
# class Lang_Category(models.Model):
#     category_name = models.CharField(max_length=255)
#
#     def __str__(self):
#         return self.category_name
#
#     class Meta:
#         verbose_name_plural = "Til Kategoriyalari"
#
#
# class AboutLanguage(models.Model):
#     lang_name = models.ForeignKey(Language, on_delete=models.CASCADE, related_name='name_lang')
#     categor_name = models.ForeignKey(Lang_Category, on_delete=models.CASCADE, related_name='cat_name')
#     course_lang = models.CharField(max_length=255)
#     course_date = models.CharField(max_length=255)
#     course_shakli = models.CharField(max_length=255)
#     course_price = models.CharField(max_length=255)
#
#     def __str__(self):
#         return f"{self.lang_name} | {self.categor_name} | {self.course_shakli}"
#
#     class Meta:
#         verbose_name_plural = "Kurs Malumotlari"




