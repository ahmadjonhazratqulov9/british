from django.urls import path
from .views import IndexView, AboutView, CoursesView, ContactView, Message


urlpatterns = [
    path('', IndexView, name='index'),
    path('about/', AboutView, name='about'),
    path('courses/', CoursesView, name='courses'),
    path('contact/', ContactView, name='contact'),
    path('message/', Message, name='message'),



]